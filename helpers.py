import json,requests,vinlib,_json,csv,random,re,os
from prefixes import *

def get_city_opendata(city, country='us'):
    # tmp = 'https://public.opendatasoft.com/api/records/1.0/search/?dataset=worldcitiespop&q=%s&sort=population&facet=country&refine.country=%s'
    # cmd = tmp % (city, country)
    # res = requests.get(cmd)
    # dct = json.loads(res.content)
    # out = dct['records'][0]['fields']
    #
    import geocoder,us
    g = geocoder.osm(city)
    g = g.json
    st = g["state"]
    st=us.states.lookup(st).abbr
    return st

def get_available_makes():
    make_list=[]
    with open('Year-Make-Model.csv',encoding='latin-1') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            if row[1].lower() in make_list:
                pass
            else:
                make_list.append(row[1].lower())
    return make_list

def search_for_make(make):
    if make.lower() in get_available_makes():
        return True
    else:
        return False

def get_models_make_year(make,year):
    models_list=[]
    with open('Year-Make-Model.csv',encoding='latin-1') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:

            if make.lower() == row[1].lower() and year == row[0]:
                models_list.append(row[2].lower())
    return models_list

def get_models_make(make):
    models_list=[]
    with open('Year-Make-Model.csv',encoding='latin-1') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            if row[2].lower() in models_list:
                pass
            else:
                if make.lower() == row[1].lower():
                    models_list.append(row[2].lower())
    return models_list



def mongo_kv(main_request,key,value):
    try:
        print('before getting value')
        mongodb_saved = main_request.get('sessionInfo').get('parameters').get('mongodb_saved')
        mongodb_saved_be = requests.get(backend_URL + '/db_get_all_fe/'+str(int(mongodb_saved["_id"])))
        print('new is ',mongodb_saved_be.json())
        mongodb_saved=mongodb_saved_be.json()['result']
        print('is ',mongodb_saved)
    except Exception as e:
        print('error is ',e)
        mongodb_saved= {}

    # if not mongodb_saved:
    #     mongodb_saved={}
    #     # get id here then assign id+1 to next _id
    #     id=get_max_id()+1
    #     # mongodb_saved["_id"]=random.randint(0,100000000)
    #     mongodb_saved["_id"]=id

    try:
        val_list=mongodb_saved[key]
    except:
        val_list=[]

    if key != "_id":
        val_list.append(value)
        mongodb_saved[key] = val_list
    else:
        mongodb_saved["_id"]=value
    print('before saving')
    p=requests.post(backend_URL+'/db_replace',json=mongodb_saved)
    return mongodb_saved



def mongo_list(main_request, listname, key, value,newdriver=False):
    mongodb_saved = main_request.get('sessionInfo').get('parameters').get('mongodb_saved')
    try:
        if mongodb_saved[listname]:
            i=len(mongodb_saved[listname])-1
            if mongodb_saved[listname][i] != None:
                try:
                    val_list = mongodb_saved[listname][i][key]
                except:
                    val_list = []
                val_list.append(value)
                mongodb_saved[listname][i][key]= val_list
                p = requests.post(backend_URL + '/db_replace', json=mongodb_saved)
                # replace mongodb_saved after modified from previous line
    except:
        mongodb_saved[listname]=[]
        mongodb_saved[listname].append({key: value})
        p=requests.post(backend_URL+'/db_replace',json=mongodb_saved)

    return mongodb_saved


def add_driver(main_request,listname):
    mongodb_saved = main_request.get('sessionInfo').get('parameters').get('mongodb_saved')
    mongodb_saved[listname].append({})
    return mongodb_saved

def get_coverage_level(payment):
    payment=int(payment)
    if payment >= 15000 and payment < 25000 :
        return '$15,000 to $25,000 '

    elif payment >= 25000 and payment < 50000 :
        return '$25,000 to $50,000'

    elif payment >= 50000 and payment < 100000 :
        return '$50,000 to $100,000'

    elif payment >= 100000 and payment <= 300000 :
        return '$100,000 to $300,000 or higher'
    else:
        return False

def check_email(email):
    regex = '^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$'

    if (re.search(regex, email)):
        print("Valid Email")
        return True
    else:
        print("Invalid Email")
        return False

db_keys=['01-z','02-fn','03-ln','04-mn','05-bd','06-sa','08-c','07-st','09-vin?','010-vin','011-cy','012-cmk','013-cmo','014-cpu','015-rs','016-cmws','017-cam','018-cof','019-ctd','020-g','021-ms','022-el','023-es','024-o','025-ssn','026-pr','027-res','028-dl','029-la','030-ls?','031-da','032-ide','033-ida','034-dd','035-ddd','036-dv?','037-dv','038-aad','039-il3y','040-bil','041-mc','042-mpc','043-e','044-pn','045-ad?','046-ls','047-lnum','048-ps','049-no','050-mp','051-ccn']

def last_key(main_request):
  try:
    mongodb_saved = main_request.get('sessionInfo').get('parameters').get('mongodb_saved')
  except:
    return db_keys[0]
  for i in mongodb_saved:
    if i != '_id':
      j = i
  last = db_keys.index(j)
  return db_keys[last]


def get_max_id():
    id_request = requests.get(backend_URL + '/max_id')
    id_request = id_request.json()
    id_request = id_request["_id"]
    return id_request


def upload_blob(source_file_name, destination_blob_name,bucket_name='avaya-recordings'):
    from google.cloud import storage
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "robo-agent-1b3b40dd16f2.json"
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)
    blob.upload_from_filename(source_file_name)
    result="File {} uploaded to {}".format(
            source_file_name, destination_blob_name
        )
    gcs_uri="gs://"+bucket_name+"/"+destination_blob_name
    public_url="https://storage.cloud.google.com/"+bucket_name+"/"+destination_blob_name

    gcs = {}
    gcs["result"] = result
    gcs["gcs_uri"] = gcs_uri
    gcs["public_url"] = public_url

    return gcs

def transcribe_gcs_with_word_time_offsets(gcs_uri):
    from google.cloud import speech
    import re
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "robo-agent-1b3b40dd16f2.json"
    client = speech.SpeechClient()
    audio = speech.RecognitionAudio(uri=gcs_uri)
    # encoding = enums.RecognitionConfig.AudioEncoding.ENCODING_UNSPECIFIED
    config = speech.RecognitionConfig(
        encoding=speech.RecognitionConfig.AudioEncoding.ENCODING_UNSPECIFIED,
        sample_rate_hertz=16000,
        language_code="en-US",
        enable_word_time_offsets=True,
        enable_automatic_punctuation=True
    )

    operation = client.long_running_recognize(config=config, audio=audio)

    print("Waiting for operation to complete...")
    result = operation.result(timeout=90)
    timestamps = []
    conversation_id=0
    for result in result.results:
        alternative = result.alternatives[0]

        if alternative.transcript.replace(' ','').replace(',','').replace('.','').replace('?','').lower().find('conversationid') != -1:
            x = re.findall('[0-9]+', alternative.transcript)
            if x:
                conversation_id=int(x[0])
        for word_info in alternative.words:
            if word_info.word.find('?') != -1 :
                word = word_info.word
                start_time = word_info.start_time
                timestamps.append(start_time.total_seconds())
                end_time = word_info.end_time
    return conversation_id,timestamps
