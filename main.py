from flask import Flask, request, make_response, jsonify,render_template,session
from flask_cors import CORS
from flask_pymongo import pymongo
from zang.inboundxml import Response, Say, Voice, Language, Dial,Redirect
from requests.auth import HTTPBasicAuth
from pages_handlers import *
import flask,requests,json,os


app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app.secret_key='appsecretkey'
app.config['CORS_HEADERS'] = 'Content-Type'
cors = CORS(app, resources={"/db_get_all": {"origins": "*"},"/db_delete":{"origins": "*"}})

# CORS(app)



@app.route('/webhook', methods=['POST'])
def webhook():
    main_request = request.get_json(force=True)
    tag=main_request.get('fulfillmentInfo').get('tag')
    # print(main_request)
    if tag == 'conversation-id':
        return page_conversation_id(main_request)

    if tag == 'zip-code':
        return page_zip_code(main_request)

    if tag == 'zip-code-no-match':
        return page_zip_code_no_match(main_request)

    elif tag == 'name-first':
        return page_name_first(main_request)

    elif tag == 'name-first-no-match':
        return page_name_first_no_match(main_request)

    elif tag == 'name-last':
        return page_name_last(main_request)

    elif tag == 'name-last-no-match':
        return page_name_last_no_match(main_request)

    elif tag == 'name-middle':
        return page_name_middle(main_request)

    elif tag == 'name-middle-no-match':
        return page_name_middle_no_match(main_request)

    elif tag == 'born-date':
        return page_born_date(main_request)

    elif tag == 'born-date-no-match':
        return page_born_date_no_match(main_request)

    elif tag == 'street-address':
        return page_street_address(main_request)

    elif tag == 'street-address-no-match':
        return page_street_address_no_match(main_request)

    elif tag == 'state':
        return page_state(main_request)

    elif tag == 'state-no-match':
        return page_state_no_match(main_request)

    elif tag == 'city':
        return page_city(main_request)

    elif tag == 'city-no-match':
        return page_city_no_match(main_request)

    elif tag == 'vin-question':
        return yes_no(main_request)

    elif tag == 'vin-number-yes':
        return page_vin_question_yes(main_request)

    elif tag == 'vin-number-no':
        return page_vin_question_no(main_request)

    elif tag == 'vin-number':
        return page_vin_number(main_request)

    elif tag == 'car-year':
        return page_car_year(main_request)

    elif tag == 'makes-list':
        return makes_list(main_request)

    elif tag == 'car-make':
        return page_car_make(main_request)

    elif tag == 'models-list':
        return models_list(main_request)

    elif tag == 'car-model':
        return page_car_model(main_request)

    elif tag == 'primary-list':
        return primary_list(main_request)

    elif tag == 'car-primary-use':
        return page_car_primary_use(main_request)

    elif tag == 'car-ride-sharing-question':
        return yes_no(main_request)

    elif tag == 'car-ride-sharing-yes':
        return page_car_ride_sharing_yes(main_request)

    elif tag == 'car-ride-sharing-no':
        return page_car_ride_sharing_no(main_request)

    elif tag == 'car-miles-work-school':
        return page_car_miles_work_school(main_request)

    elif tag == 'car-annual-mileage':
        return page_car_annual_mileage(main_request)

    elif tag == 'car-own-finance':
        return page_car_own_finance(main_request)

    elif tag == 'car-tracking-device-yes':
        return page_car_tracking_device_yes(main_request)

    elif tag == 'car-tracking-device-no':
        return page_car_tracking_device_no(main_request)

    elif tag == 'gender':
        return page_gender(main_request)

    elif tag == 'marital-status':
        return page_marital_status(main_request)

    elif tag == 'education-level':
        return page_education_level(main_request)

    elif tag == 'employment-status-employed':
        return page_employment_status_employed(main_request)

    elif tag == 'employment-status':
        return page_employment_status(main_request)

    elif tag == 'occupation':
        return page_occupation(main_request)

    elif tag == 'social-security-number':
        return page_social_security_number(main_request)

    elif tag == 'primary-residence':
        return page_primary_residence(main_request)

    elif tag == 'residence-status':
        return page_residence_status(main_request)

    elif tag == 'driver-license-yes':
        return page_driver_license_yes(main_request)

    elif tag == 'driver-license-no':
        return page_driver_license_no(main_request)

    elif tag == 'license-age':
        return page_license_age(main_request)

    elif tag == 'license-status-yes':
        return page_license_status_yes(main_request)

    elif tag == 'license-status-no':
        return page_license_status_no(main_request)

    elif tag == 'driver-accidents-yes':
        return page_driver_accidents_yes(main_request)

    elif tag == 'driver-accidents-no':
        return page_driver_accidents_no(main_request)

    elif tag == 'incident-description':
        return page_incident_description(main_request)

    elif tag == 'incident-date':
        return page_incident_date(main_request)

    elif tag == 'dui-dwi-yes':
        return page_dui_dwi_yes(main_request)

    elif tag == 'dui-dwi-no':
        return page_dui_dwi_no(main_request)

    elif tag == 'dui-dwi-incident-date':
        return page_dui_dwi_incident_date(main_request)

    elif tag == 'driver-violations':
        return page_driver_violations(main_request)

    elif tag == 'driver-violations-yes':
        return page_driver_violations_yes(main_request)

    elif tag == 'driver-violations-no':
        return page_driver_violations_no(main_request)

    elif tag == 'add-another-driver-yes':
        return page_add_another_driver_yes(main_request)

    elif tag == 'add-another-driver-no':
        return page_add_another_driver_no(main_request)

    elif tag == 'insurance-last-3-years-yes':
        return page_insurance_last_3_years_yes(main_request)

    elif tag == 'insurance-last-3-years-no':
        return page_insurance_last_3_years_no(main_request)

    elif tag == 'body-injury-limits':
        return page_body_injury_limits(main_request)

    elif tag == 'motorist-coverage':
        return page_motorist_coverage(main_request)

    elif tag == 'medical-payments-coverage':
        return page_medical_payments_coverage(main_request)

    elif tag == 'domain':
        return page_domain(main_request)

    elif tag == 'email':
        return page_email(main_request)

    elif tag == 'email-spelling':
        return page_email_spelling(main_request)

    elif tag == 'phone-number':
        return page_phone_number(main_request)

    elif tag == 'autodialed-yes':
        return page_autodialed_yes(main_request)

    elif tag == 'autodialed-no':
        return page_autodialed_no(main_request)

    elif tag == 'license-state':
        return page_license_state(main_request)

    elif tag == 'license-numbers' or tag == 'license-numbers-letters' :
        return page_license_number(main_request)

    elif tag == 'policy-start':
        return page_policy_start(main_request)

    elif tag == 'national-origin':
        return page_national_origin(main_request)

    elif tag == 'monthly-pay':
        return page_monthly_pay(main_request)

    elif tag == 'credit-card-number':
        return page_credit_card_number(main_request)

@app.route('/messenger')
def messenger():
    return render_template('messenger.html')


@app.route('/db_login', methods=['POST'])
def login():
    cred = request.json
    # print(cred)
    user = cred['username']
    password = cred['password']
    dbname = "robo_agent_db"
    dbstring = "mongodb+srv://" + user + ':' + password + "@cluster0.kmuyv.mongodb.net/" + dbname + "?retryWrites=true&w=majority"
    # try:
    client = pymongo.MongoClient(dbstring)
    db = client.get_database(dbname)
    records = db.robo_agent_db_c
    app.config["records"] = records
    instance=get_all_fe_instances(1)
    return jsonify({'result': 'logged'})
    # except:
    #     return jsonify({'result': 'failed'})

@app.route('/db_get_all')
def get_all_instances():
    try:
        login_check()
        records = app.config["records"]
    except:
        return jsonify({"result": 'not logged'})

    instance_cursor = records.find({})
    print(instance_cursor)
    instances = []
    for instance in instance_cursor:
        id = instance['_id']
        del instance['_id']
        instances.append(instance)
    bots=[]
    for i in instances:
        i=dict(sorted(i.items(),reverse=True))
        bots.append(i)
    response=jsonify({"result": bots})
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response

@app.route('/db_get_all_fe/<id>')
def get_all_fe_instances(id):
    try:
        login_check()
        records = app.config["records"]
    except:
        return jsonify({"result": 'not logged'})
    try:
        id=int(id)
    except:
        return jsonify({"result": False})
    instance_cursor = records.find({"_id":id})
    # print(instance_cursor)
    instances = []
    for instance in instance_cursor:
        id = instance['_id']
        instances=instance

    mod_bot={}
    for i in instances:

        if i == '20-drivers':
            # print(instances["20-drivers"])
            for index,j in enumerate(instances["20-drivers"]):
                for k in j:
                    keyname='20-drivers-'+ str(index+1)+'-'+str(k)
                    mod_bot[keyname]=j[k]

        else:
            mod_bot [i] = instances[i]

    if instances:
        mod_bot = dict(sorted(mod_bot.items(), reverse=True))


    response=jsonify({"result": mod_bot})
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response



@app.route('/db_replace_key', methods=['POST'])
def replace_key():
    try:
        login_check()
        records = app.config["records"]
    except:
        return jsonify({"result": 'not logged'})
    data = request.json
    print(data)
    id={"_id":data["_id"]}
    cursor = records.find(id)
    for c in cursor:
        answer=c

    del data["_id"]
    key = list(data.keys())[0]

    answer[key]=data[key]

    answers_cursor = records.replace_one(id,answer,upsert=False)
    result =answers_cursor.raw_result["updatedExisting"]
    return jsonify({"results":result})



@app.route('/db_delete')
def delete_instances():
    try:
        login_check()
        records = app.config["records"]
    except:
        return jsonify({"result": 'not logged'})

    result = records.delete_many({})
    response=jsonify({"deleted_count":result.deleted_count})
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@app.route('/db_replace', methods=["POST"])
def replace_instance():
    try:
        login_check()
        records = app.config["records"]
    except:
        return jsonify({"result": 'not logged'})
    data = request.json
    id={"_id":data["_id"]}
    answers_cursor = records.replace_one(id,data,upsert=True)
    result =answers_cursor.raw_result["updatedExisting"]
    return jsonify({"results":result})

@app.route('/max_id', methods=['GET','POST'])
def next_id():
    try:
        login_check()
        records = app.config["records"]
    except:
        return jsonify({"result": 'not logged'})

    # to get maximum value of id in dtabase
    try:
        id_cursor=records.find_one(sort=[("_id", -1)])["_id"]
        # if id_cursor:
        return jsonify({"_id": id_cursor})
    except:
        return jsonify({"_id": 0})

@app.route('/db_add_keys', methods=['POST'])
def add_keys():
    try:
        login_check()
        records = app.config["records"]
    except:
        return jsonify({"result": 'not logged'})
    data = request.json
    id = {"_id": data["_id"]}
    cursor = records.find(id)
    for c in cursor:
        answer = c

    del data["_id"]
    keys = list(data.keys())
    db_keys=list(answer.keys())
    for i in keys:
        if (i not in db_keys):
            answer[i]=''
        if answer[i] != data[i]:
            answer[i]=data[i]

    answers_cursor = records.replace_one(id, answer, upsert=False)
    result = answers_cursor.raw_result["updatedExisting"]
    return jsonify({"results": result})

def login_check(user="patrickjcross",password="Patrick1"):
    try:
        records = app.config["records"]
    except:
        dbname = "robo_agent_db"
        dbstring = "mongodb+srv://" + user + ':' + password + "@cluster0.kmuyv.mongodb.net/" + dbname + "?retryWrites=true&w=majority"
        client = pymongo.MongoClient(dbstring)
        db = client.get_database(dbname)
        records = db.robo_agent_db_c
        # print(records.find({}))
        app.config["records"] = records
        # print(get_all_instances())


@app.route('/make_call/<number>', methods=['GET','POST'])
def call(number):
    auth=HTTPBasicAuth(username=account_sid,password=authtoken)
    gateway='https://api.zang.io'
    url=gateway+'/v2/Accounts/AC777c3e329afbabdd1ba141e69df65e53/Calls.json'
    params={
        "From": "+15102281128",
        "To": str(number),
        "Url": backend_URL+"/callback_url",
        "Record": True
    }
    try:
        caller=requests.post(url=url,params=params,auth=auth)
        code=caller.status_code
        caller=caller.json()
        call_sid=caller["sid"]
        url=backend_URL+'/call_recordings/'+str(call_sid)
    except:
        return jsonify({"result":False})

    if code == 400:
        return jsonify({"result":False})
    else:
        return jsonify({
            "result":True,
            "recording_url":url,
            "description":"visit this link after you end the call to get call details"
        })

@app.route('/call_recordings/<call_sid>', methods=['GET','POST'])
def get_call_recording(call_sid):
    url='https://api.zang.io/v2/Accounts/'+account_sid+'/Recordings.json'
    auth=HTTPBasicAuth(username=account_sid,password=authtoken)
    recordings_list=requests.get(url=url,auth=auth)
    recordings_list=recordings_list.json()["recordings"]
    call_time=''
    result=''
    for i in recordings_list:
        if i["call_sid"] == call_sid:
            recording_url=i["recording_url"]
            call_time=i["date_created"]
            if recording_url != '':
                print(recording_url)
                filename = recording_url.split('/')[len(recording_url.split('/')) - 1].split('.')[0]
                dir = '/tmp/'
                source_filename = dir + filename + '.mp3'
                doc = requests.get(recording_url)
                print('files stored in /tmp/ folder is ',os.listdir(dir))
                with open(source_filename, 'wb') as f:
                    f.write(doc.content)
                result = upload_blob(source_file_name=source_filename, destination_blob_name=filename)
                print('files stored in /tmp/ folder is ',os.listdir(dir))
                for f in os.listdir(dir):
                    if str(f) == filename+'.mp3':
                        os.remove(os.path.join(dir, f))
                print('files stored in /tmp/ folder is ',os.listdir(dir))
            break

    if result != '':
        response = result
        gcs_uri = response["gcs_uri"]
        conversation_id,timestamps=transcribe_gcs_with_word_time_offsets(gcs_uri)
        # if len(timestamps) >= 1:
        url=backend_URL+'/db_add_keys'
        body={}
        body["_id"]=int(conversation_id)
        body["public_url"]=response["public_url"]
        # body["gcs_uri"]=response["gcs_uri"]
        body["question_timestamps"]=timestamps
        r=requests.post(url=url,json=body)
        response["timestamps"]=timestamps
        response['conversation_id']=conversation_id
        response["call_start_time"]=call_time
        return jsonify(response)
    else:
        return jsonify({"result":"no recordings found"})


@app.route('/callback_url', methods=['GET','POST'])
def callback_responder():
    print(request.json)
    avaya_n = '+15102281128'
    redirect=Redirect(url=voice_call_xml)
    response = Response()
    response.addElement(redirect)
    xml=response.xml
    return flask.Response(xml,mimetype="text/xml")

@app.route('/fallback_url', methods=['GET','POST'])
def fallback_responder():
    print(request.json)
    say = Say("problem happened here",
              language=Language.EN,
              voice=Voice.FEMALE)
    response = Response()
    response.addElement(say)
    xml=response.xml
    return flask.Response(xml,mimetype="text/xml")





if __name__ == '__main__':
    # session['data']={}
    app.run(debug=True,threaded=True)
